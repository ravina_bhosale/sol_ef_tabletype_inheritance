﻿using Sol_EF_TableType_Inheritance.Entity;
using Sol_EF_TableType_Inheritance.Entity.Admin;
using Sol_EF_TableType_Inheritance.Repository.Admin;
using Sol_EF_TableType_Inheritance.Repository.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_TableType_Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<UserLoginEntity> listEmployeeObj =
                   await new UserRepository().GetUserData();



                IEnumerable<AdminLoginEntity> listVendorObj =
                    await new AdminRepository().GetAdminData();

            }).Wait();
        }
    }
}
