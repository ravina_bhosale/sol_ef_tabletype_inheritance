﻿using Sol_EF_TableType_Inheritance.EF;
using Sol_EF_TableType_Inheritance.Entity;
using Sol_EF_TableType_Inheritance.Repository.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_TableType_Inheritance.Repository.User
{
   public class UserRepository : PersonRepository
    {

        #region Constructor
        public UserRepository() : base()
        {

        }
        #endregion

        #region Public Method
        public async Task<IEnumerable<UserLoginEntity>> GetUserData()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                       base.DbObject
                        ?.tblPersons
                        ?.OfType<tbl_User>()
                        ?.AsEnumerable()
                        ?.Select(this.SelectUserData)
                        ?.ToList();

                    return getQuery;

                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<tbl_User,UserLoginEntity> SelectUserData
        {
            get
            {
                return
                    (letblUserObj) => new UserLoginEntity()
                    {
                        PersonId=letblUserObj.PersonId,
                        UserName=letblUserObj.UserName,
                        Password=letblUserObj.Password,
                        MobileNo=letblUserObj.MobileNo,
                        FirstName=letblUserObj.FirstName
                        
                    };
            }
        }

        #endregion
    }
}
