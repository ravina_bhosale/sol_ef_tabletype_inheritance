﻿using Sol_EF_TableType_Inheritance.EF;
using Sol_EF_TableType_Inheritance.Entity.Admin;
using Sol_EF_TableType_Inheritance.Repository.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_TableType_Inheritance.Repository.Admin
{
   public class AdminRepository : PersonRepository
    {

        #region Constructor
        public AdminRepository() : base()
        {

        }
        #endregion

        #region Public Method
        public async Task<IEnumerable<AdminLoginEntity>> GetAdminData()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                       base.DbObject
                        ?.tblPersons
                        ?.OfType<tbl_Admin>()
                        ?.AsEnumerable()
                        ?.Select(this.SelectAdminData)
                        ?.ToList();

                    return getQuery;

                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<tbl_Admin, AdminLoginEntity> SelectAdminData
        {
            get
            {
                return
                    (letblAdminObj) => new AdminLoginEntity()
                    {
                        PersonId = letblAdminObj.PersonId,
                        AdminName = letblAdminObj.AdminName,
                        Password = letblAdminObj.Password,
                        FirstName=letblAdminObj.FirstName,
                        LastName=letblAdminObj.LastName

                    };
            }
        }

        #endregion
    }
}
