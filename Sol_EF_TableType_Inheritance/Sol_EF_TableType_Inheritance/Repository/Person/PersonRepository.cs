﻿using Sol_EF_TableType_Inheritance.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_TableType_Inheritance.Repository.Person
{
   public class PersonRepository
    {
        #region Declaration
        private PersonDBEntities db = null;
        #endregion

        #region Constructor
        public PersonRepository()
        {
            db = new PersonDBEntities();

            this.DbObject = db;
        }
        #endregion

        #region Property
       public  PersonDBEntities DbObject { get; set; }
        #endregion
    }
}
