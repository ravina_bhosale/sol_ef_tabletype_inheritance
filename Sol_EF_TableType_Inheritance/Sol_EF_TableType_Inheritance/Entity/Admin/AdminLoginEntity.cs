﻿using Sol_EF_TableType_Inheritance.Entity.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_TableType_Inheritance.Entity.Admin
{
   public class AdminLoginEntity :PersonEntity
    {
       public String AdminName { get; set; }

        public String Password { get; set; }

    }
}
