﻿using Sol_EF_TableType_Inheritance.Entity.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_TableType_Inheritance.Entity
{
    public class UserLoginEntity:PersonEntity
    {

        public String UserName { get; set; }

        public String Password { get; set; }
    }
}
